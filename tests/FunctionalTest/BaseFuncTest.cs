﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FunctionalTest
{
    public class BaseFuncTest : IDisposable
    {
        public readonly TestServer _server;
        public readonly HttpClient _client;

        protected BaseFuncTest()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<FunctionalTest.Startup>());
            _client = _server.CreateClient();
        }

        public void Dispose()
        {

        }
    }
}
