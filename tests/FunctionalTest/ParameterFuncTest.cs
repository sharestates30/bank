﻿using Api.ViewModels;
using FunctionalTest.DataBuilder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FunctionalTest
{
    public class ParameterFuncTest : BaseFuncTest
    {
        [Fact]
        public async Task Create_And_Load_Parameter_Test()
        {
            // Arrange
            var model = new ParameterBuilder()
                .WithCode("tableCode")
                .WithName("Param 01")
                .WithValue("04");

            // Act
            var response = await _client.PostAsync("/parameter", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            if (response.IsSuccessStatusCode) {
                // Get location
                var resourceEntity = response.Headers.Location.ToString();
                // Load resource
                var responseEntity = await _client.GetAsync(resourceEntity);
                Assert.Equal(HttpStatusCode.OK, responseEntity.StatusCode);
            }

        }

        [Fact]
        public async Task Create_And_Delete_Parameter_Test()
        {
            // Arrange
            var model = new ParameterBuilder()
                .WithCode("tableCode")
                .WithName("Param 01")
                .WithValue("04");

            // Act
            var response = await _client.PostAsync("/parameter", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            if (response.IsSuccessStatusCode)
            {
                // Get location
                var resourceEntity = response.Headers.Location.ToString();
                // Delete resource
                response = await _client.DeleteAsync(resourceEntity);
                Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

                // Validate if resource was deleted
                response = await _client.GetAsync(resourceEntity);
                Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            }

        }

        [Fact]
        public async Task Create_And_Update_Parameter_Test()
        {
            // Arrange
            var model = new ParameterBuilder()
                .WithCode("tableCode")
                .WithName("Param 01")
                .WithValue("04");

            // Act
            var response = await _client.PostAsync("/parameter", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            if (response.IsSuccessStatusCode)
            {
                // Get location
                var resourceEntity = response.Headers.Location.ToString();
                response = await _client.GetAsync(resourceEntity);
                var entityJson = await response.Content.ReadAsStringAsync();
                var entity = Newtonsoft.Json.JsonConvert.DeserializeObject<ParameterViewModel>(entityJson);

                // Update resource
                var modelUpdate = new ParameterBuilder()
                    .WithCode(entity.Code)
                    .WithName(entity.Name)
                    .WithValue($"{entity.Value} updated");

                response = await _client.PutAsync(resourceEntity, new StringContent(JsonConvert.SerializeObject(modelUpdate), Encoding.UTF8, "application/json"));
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);

                // Validate if resource was updated
                response = await _client.GetAsync(resourceEntity);
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);

                entityJson = await response.Content.ReadAsStringAsync();
                entity = Newtonsoft.Json.JsonConvert.DeserializeObject<ParameterViewModel>(entityJson);
                Assert.Equal(entity.Value, modelUpdate.Value);

            }

        }
    }
}
