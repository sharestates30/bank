﻿using Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FunctionalTest.DataBuilder
{
    public class InvestorBuilder
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Logo { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string EIN { get; set; }
        public int FoundedIn { get; set; }
        public string WebSite { get; set; }
        public int NumberYearBusiness { get; set; }
        public int NumberFullTimeEmployees { get; set; }
        public string PrimaryMarkets { get; set; }
        public int FundingType { get; set; }
        public bool RaisedMoney { get; set; }
        public string NotableProjectAddress { get; set; }
        public string NotableProjectName { get; set; }
        public string AverageTransactionSize { get; set; }
        public string AssetManagement { get; set; }
        public decimal SponsorTrackRecord { get; set; }
        public string ExtraInformation { get; set; }
        public List<int> ProjectDeveloped { get; set; }

        public InvestorBuilder WithEmail(string email)
        {
            this.Email = email;
            return this;
        }

        public InvestorBuilder WithFirstName(string name)
        {
            this.FirstName = name;
            return this;
        }

        public InvestorBuilder WithLastName(string name)
        {
            this.LastName = name;
            return this;
        }

        public InvestorBuilder WithProjects(int value)
        {
            if (this.ProjectDeveloped == null)
                this.ProjectDeveloped = new List<int>();

            this.ProjectDeveloped.Add(value);

            return this;
        }

    }
}
