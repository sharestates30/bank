﻿using Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FunctionalTest.DataBuilder
{
    public class ParameterBuilder
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsActive { get; set; }

        public ParameterBuilder WithCode(string code)
        {
            this.Code = code;
            return this;
        }

        public ParameterBuilder WithName(string name)
        {
            this.Name = name;
            return this;
        }

        public ParameterBuilder WithValue(string value)
        {
            this.Value = value;
            return this;
        }
        

    }
}
