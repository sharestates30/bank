﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Entity.Configuration;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.Swagger.Model;
using Core;
using IdentityServer4;
using System.IO;
using Sharestates.Logger.Core;
using Api.Services;

namespace Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            SharestatesLogger.Configure(env, Configuration);
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddCors();
            services.AddMvc();

            services.Configure<AppOptions>(Configuration.GetSection("Setting"));

            // Add Api Docs.
            //var pathToDoc = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Api.xml");
            //services.AddSwaggerGen();
            //services.ConfigureSwaggerGen(options =>
            //{
            //    options.SingleApiVersion(new Info
            //    {
            //        Version = "v1",
            //        Title = "API",
            //        Description = "Api",
            //        TermsOfService = "None"
            //    });
            //    options.IncludeXmlComments(pathToDoc);
            //    options.DescribeAllEnumsAsStrings();
            //});

            // AppSettings.
            services.AddDbContext<BankDbContext>(options => options.UseSqlServer(Configuration["Data:ConnectionString"]));

            services.AddTransient<IBankRoutingNumberService, BankRoutingNumberService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseSharestatesLogger(loggerFactory, env);

            app.UseStaticFiles();

            app.UseCors(builder => builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod());

            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = Configuration.GetValue<string>("Authority"),
                RequireHttpsMetadata = false,
                AllowedScopes = {
                    "api",
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    IdentityServerConstants.StandardScopes.OfflineAccess },
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
            });
            app.UseMvc();
        }
    }
}
