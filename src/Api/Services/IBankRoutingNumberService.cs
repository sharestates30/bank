﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services
{
    public interface IBankRoutingNumberService
    {
        bool IsValidRoutingNumber(string routingNumber);
        Task<BankRoutingNumberModel> GetRoutingNumberInfo(string routingNumber);
    }
}
