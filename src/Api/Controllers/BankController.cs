﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{
    [Authorize]
    public class BankController : BaseController
    {
        private readonly BankDbContext _dbContext;

        public BankController(BankDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        [HttpGet("banks")]
        [ProducesResponseType(typeof(BankViewModel), 200)]
        public IActionResult Get(Guid referenceId)
        {
            using (var context = this._dbContext)
            {
                var query = from c in context.Banks
                             select new BankViewModel
                             {
                                 BankId = c.BankId,
                                 Name = c.Name,
                                 Description = c.Description
                             };
                
                var model = query.ToList();

                return this.Ok(model);
            }
        }
        
    }
}

