﻿using Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog.Events;
using Sharestates.Logger.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    public class SharestatesLoggerController : Controller
    {
        [HttpPatch("logger")]
        [AllowAnonymous]
        public IActionResult SwitchLoggingLevel([FromBody]LoggerModel model)
        {
            SharestatesLogger.LevelSwitch.MinimumLevel = (LogEventLevel)model.Level;
            return Ok();
        }
    }
}
