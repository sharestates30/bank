﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Api.Services;

namespace Api.Controllers
{
    [Authorize]
    public class BankAccountController : BaseController
    {
        private readonly BankDbContext _dbContext;
        private readonly IBankRoutingNumberService _bankRoutingNumberService;

        public BankAccountController(BankDbContext dbContext, IBankRoutingNumberService bankRoutingNumberService)
        {
            this._dbContext = dbContext;
            this._bankRoutingNumberService = bankRoutingNumberService;
        }

        [HttpGet("account/{referenceId}/bankAccounts")]
        [ProducesResponseType(typeof(BankAccountViewModel), 200)]
        public IActionResult Get(Guid referenceId)
        {
            using (var context = this._dbContext)
            {
                var query = from c in context.BankAccounts
                                .Where(c=> c.ReferenceId.Equals(referenceId))
                                .Include(c=> c.Bank)
                             select new BankAccountViewModel
                             {
                                 BankAccountId = c.BankAccountId,
                                 BankAccountType = (int)c.BankAccountType,
                                 BankAccountTypeName = c.BankAccountType.ToString(),
                                 BankName = c.Bank.Name,
                                 AccountName = c.AccountName,
                                 AccountNumber = c.AccountNumber,
                                 AssociateAccount = c.AssociateAccount,
                                 AssociateAccountName = c.AssociateAccountName,
                                 AssociateAccountType = c.AssociateAccountType,
                                 ReferenceId = c.ReferenceId,
                                 ReferenceName = c.ReferenceName,
                                 ReferenceType = c.ReferenceType,
                                 RoutingNumber = c.RoutingNumber,
                                 CreatedOn = c.CreatedOn
                             };
                
                var model = query.ToList();

                return this.Ok(model);
            }
        }
        
        [HttpGet("account/{referenceId}/bankAccounts/{id}", Name = "GetBankAccountById")]
        public IActionResult GetById(Guid referenceId, Guid id)
        {
            using (var context = this._dbContext)
            {
                var model = (from c in context.BankAccounts
                                .Include(c=> c.Bank)
                             where c.BankAccountId.Equals(id)
                             select new BankAccountViewModel
                             {
                                 BankAccountId = c.BankAccountId,
                                 BankAccountType = (int)c.BankAccountType,
                                 BankAccountTypeName = c.BankAccountType.ToString(),
                                 BankName = c.Bank.Name,
                                 AccountName = c.AccountName,
                                 AccountNumber = c.AccountNumber,
                                 AssociateAccount = c.AssociateAccount,
                                 AssociateAccountName = c.AssociateAccountName,
                                 AssociateAccountType = c.AssociateAccountType,
                                 ReferenceId = c.ReferenceId,
                                 ReferenceName = c.ReferenceName,
                                 ReferenceType = c.ReferenceType,
                                 RoutingNumber = c.RoutingNumber,
                                 CreatedOn = c.CreatedOn
                             }).FirstOrDefault();

                if (model == null)
                    return this.NotFound();

                return this.Ok(model);
            }
        }

        [HttpPost("account/{referenceId}/bankAccounts")]
        public async Task<IActionResult> Post(Guid referenceId, [FromBody]BankAccountPostViewModel model)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            if (!this._bankRoutingNumberService.IsValidRoutingNumber(model.RoutingNumber))
                return this.BadRequest("Bank Routing Number is invalid.");

            using (var context = this._dbContext)
            {
                var entity = new BankAccount
                {
                    BankAccountId = Guid.NewGuid(),
                    ReferenceId = referenceId,
                    ReferenceName = model.ReferenceName,
                    ReferenceType = model.ReferenceType,
                    BankAccountType = (BankAccountTypeEnum)model.BankAccountType,
                    Bank = new Bank() { Name = model.BankName, Description = model.BankName },
                    RoutingNumber = model.RoutingNumber,
                    AccountName = model.AccountName,
                    AccountNumber = model.AccountNumber,
                    AssociateAccount = model.AssociateAccount,
                    AssociateAccountName = model.AssociateAccountName,
                    AssociateAccountType = model.AssociateAccountType,
                    CreatedOn = DateTime.UtcNow,
                    CreatedBy = this.SubjectId,
                    CreatedByName = this.SubjectName,
                };
                
                context.BankAccounts.Add(entity);
                await context.SaveChangesAsync();

                return this.Created(Url.RouteUrl("GetBankAccountById", new { referenceId = referenceId, id = entity.BankAccountId }), new { });
            }
        }

        [HttpPut("account/{referenceId}/bankAccounts/{id}")]
        public async Task<IActionResult> Put(Guid referenceId, Guid id, [FromBody]BankAccountPostViewModel model)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);


            if (!this._bankRoutingNumberService.IsValidRoutingNumber(model.RoutingNumber))
                return this.BadRequest("Bank Routing Number is invalid.");

            using (var context = this._dbContext)
            {
                var entity = context.BankAccounts.FirstOrDefault(c => c.BankAccountId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                entity.BankAccountType = (BankAccountTypeEnum)model.BankAccountType;
                entity.RoutingNumber = model.RoutingNumber;
                entity.AccountName = model.AccountName;
                entity.AccountNumber = model.AccountNumber;
                entity.AssociateAccount = model.AssociateAccount;
                entity.AssociateAccountName = model.AssociateAccountName;
                entity.AssociateAccountType = model.AssociateAccountType;
                entity.UpdatedOn = DateTime.UtcNow;
                entity.UpdatedBy = this.SubjectId;
                entity.UpdatedByName = this.SubjectName;

                context.BankAccounts.Update(entity);
                await context.SaveChangesAsync();

                return this.Ok();
            }
        }
        
        [HttpDelete("account/{referenceId}/bankAccounts/{id}")]
        public async Task<IActionResult> Delete(Guid referenceId, Guid id)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                    .BankAccounts
                    .FirstOrDefault(c => c.BankAccountId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                context.BankAccounts.Remove(entity);
                await context.SaveChangesAsync();

                return this.NoContent();
            }
        }
    }
}

