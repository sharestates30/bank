﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using Core;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class ParameterController : BaseController
    {
        private readonly BankDbContext _dbContext;

        public ParameterController(BankDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        [HttpGet("groups")]
        public IActionResult GetGroups()
        {
            var model = new List<ParameterGroupViewModel>();

            return this.Ok(model);
        }

        [HttpGet]
        public IActionResult Get([FromQuery(Name = "code")]string code)
        {
            using (var context = this._dbContext)
            {
                var model = (from c in context.Parameters
                             select new ParameterViewModel
                             {
                                 ParameterId = c.ParameterId,
                                 Code = c.Code,
                                 Name = c.Name,
                                 Value = c.Value,
                                 IsActive = c.IsActive
                             });

                if (!string.IsNullOrEmpty(code)) {
                    var codeSplit = code.Split(',');
                    model = model.Where(c => codeSplit.Contains(c.Code));
                }

                return this.Ok(model.ToList());
            }
        }

        [HttpGet("{id}", Name = "GetParameterById")]
        public IActionResult GetById(int id)
        {
            using (var context = this._dbContext)
            {
                var model = (from c in context.Parameters
                             where c.ParameterId.Equals(id)
                             select new ParameterViewModel
                             {
                                 ParameterId = c.ParameterId,
                                 Code = c.Code,
                                 Name = c.Name,
                                 Value = c.Value,
                                 IsActive = c.IsActive
                             }).FirstOrDefault();

                if (model == null)
                    return this.NotFound();

                return this.Ok(model);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ParameterPostViewModel model)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            using (var context = this._dbContext)
            {
                var entity = new Parameter
                {
                    Code = model.Code,
                    Name = model.Name,
                    Value = model.Value,
                    IsActive = model.IsActive
                };

                context.Parameters.Add(entity);
                await context.SaveChangesAsync();

                return this.Created(Url.RouteUrl("GetParameterById", new { id = entity.ParameterId }), new { });
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]ParameterPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context.Parameters.FirstOrDefault(c => c.ParameterId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                entity.Code = model.Code;
                entity.Name = model.Name;
                entity.Value = model.Value;
                entity.IsActive = model.IsActive;

                context.Parameters.Update(entity);
                await context.SaveChangesAsync();

                return this.Ok();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            using (var context = this._dbContext)
            {
                var entity = context.Parameters.FirstOrDefault(c => c.ParameterId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                context.Parameters.Remove(entity);
                await context.SaveChangesAsync();

                return this.NoContent();
            }
        }
    }
}

