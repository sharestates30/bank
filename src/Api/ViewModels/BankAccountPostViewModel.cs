﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class BankAccountPostViewModel
    {
        public Guid FundingSourceId { get; set; }
        public int BankAccountType { get; set; }
        [Required]
        [StringLength(150)]
        public string BankName { get; set; }
        [Required]
        [StringLength(9)]
        public string RoutingNumber { get; set; }
        [Required]
        [StringLength(50)]
        public string AccountName { get; set; }
        [Required]
        [StringLength(17)]
        public string AccountNumber { get; set; }

        public Guid AssociateAccount { get; set; }
        public string AssociateAccountName { get; set; }
        public string AssociateAccountType { get; set; }

        public Guid ReferenceId { get; set; }
        public string ReferenceName { get; set; }
        public string ReferenceType { get; set; }
    }
}
