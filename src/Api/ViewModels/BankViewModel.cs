﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class BankViewModel
    {
        public Guid BankId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
