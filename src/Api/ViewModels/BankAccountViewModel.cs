﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class BankAccountViewModel
    {
        public Guid BankAccountId { get; set; }
        public int BankAccountType { get; set; }
        public string BankAccountTypeName { get; set; }
        public string BankName { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }

        public Guid ReferenceId { get; set; }
        public string ReferenceName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string AssociateAccountType { get; set; }
        public string AssociateAccountName { get; set; }
        public Guid AssociateAccount { get; set; }
        public string ReferenceType { get; set; }
    }
}
