﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models
{
    public class BadRequestModel
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }

    public class CreateBadRequest {

        public List<BadRequestModel> Messages { get; set; }

        public void Create(string message) {
            if (this.Messages == null)
                this.Messages = new List<BadRequestModel>();

            this.Messages.Add(new BadRequestModel { Message = message });
        }

    }
}
