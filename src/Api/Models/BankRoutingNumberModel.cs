﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models
{
    public class BankRoutingNumberModel
    {
        [JsonProperty("customer_name")]
        public string BankName { get; set; }
        [JsonProperty("zip")]
        public string PostalCode { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("routing_number")]
        public string RoutingNumber { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("telephone")]
        public string Phone { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("rn")]
        public string Rn { get; set; }
    }
}
