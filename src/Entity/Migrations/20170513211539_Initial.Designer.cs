﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Entity;

namespace Entity.Migrations
{
    [DbContext(typeof(BankDbContext))]
    [Migration("20170513211539_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Entity.Account", b =>
                {
                    b.Property<Guid>("AccountId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AddressLine1");

                    b.Property<string>("AddressLine2");

                    b.Property<string>("City");

                    b.Property<string>("CompanyTitle");

                    b.Property<string>("Country");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("EIN");

                    b.Property<string>("EntityType");

                    b.Property<DateTime?>("FormationDate");

                    b.Property<string>("Jurisdiction");

                    b.Property<string>("Name");

                    b.Property<string>("NickName");

                    b.Property<string>("PhoneNumber");

                    b.Property<Guid>("ReferenceId");

                    b.Property<string>("ReferenceName");

                    b.Property<string>("ReferenceType");

                    b.Property<string>("SSN");

                    b.Property<string>("State");

                    b.Property<string>("SubType");

                    b.Property<string>("TaxId");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.Property<string>("ZipCode");

                    b.HasKey("AccountId");

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("Entity.Bank", b =>
                {
                    b.Property<Guid>("BankId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("BankId");

                    b.ToTable("Banks");
                });

            modelBuilder.Entity("Entity.BankAccount", b =>
                {
                    b.Property<Guid>("BankAccountId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AccountName");

                    b.Property<string>("AccountNumber");

                    b.Property<Guid>("AssociateAccount");

                    b.Property<string>("AssociateAccountName");

                    b.Property<string>("AssociateAccountType");

                    b.Property<int>("BankAccountType");

                    b.Property<Guid>("BankId");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid>("ReferenceId");

                    b.Property<string>("ReferenceName");

                    b.Property<string>("ReferenceType");

                    b.Property<string>("RoutingNumber");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("BankAccountId");

                    b.HasIndex("BankId");

                    b.ToTable("BankAccounts");
                });

            modelBuilder.Entity("Entity.Parameter", b =>
                {
                    b.Property<int>("ParameterId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.Property<string>("Value");

                    b.HasKey("ParameterId");

                    b.ToTable("Parameters");
                });

            modelBuilder.Entity("Entity.BankAccount", b =>
                {
                    b.HasOne("Entity.Bank", "Bank")
                        .WithMany("BankAccounts")
                        .HasForeignKey("BankId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
