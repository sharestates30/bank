﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Entity;

namespace Entity.Migrations
{
    [DbContext(typeof(BankDbContext))]
    [Migration("20170519221347_RemoveAccount")]
    partial class RemoveAccount
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Entity.Bank", b =>
                {
                    b.Property<Guid>("BankId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("BankId");

                    b.ToTable("Banks");
                });

            modelBuilder.Entity("Entity.BankAccount", b =>
                {
                    b.Property<Guid>("BankAccountId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AccountName");

                    b.Property<string>("AccountNumber");

                    b.Property<Guid>("AssociateAccount");

                    b.Property<string>("AssociateAccountName");

                    b.Property<string>("AssociateAccountType");

                    b.Property<int>("BankAccountType");

                    b.Property<Guid>("BankId");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid>("ReferenceId");

                    b.Property<string>("ReferenceName");

                    b.Property<string>("ReferenceType");

                    b.Property<string>("RoutingNumber");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("BankAccountId");

                    b.HasIndex("BankId");

                    b.ToTable("BankAccounts");
                });

            modelBuilder.Entity("Entity.Parameter", b =>
                {
                    b.Property<int>("ParameterId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CreatedByName");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name");

                    b.Property<string>("UpdatedBy");

                    b.Property<string>("UpdatedByName");

                    b.Property<DateTime?>("UpdatedOn");

                    b.Property<string>("Value");

                    b.HasKey("ParameterId");

                    b.ToTable("Parameters");
                });

            modelBuilder.Entity("Entity.BankAccount", b =>
                {
                    b.HasOne("Entity.Bank", "Bank")
                        .WithMany("BankAccounts")
                        .HasForeignKey("BankId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
