﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BankAccount : BaseEntity
    {
        public Guid BankAccountId { get; set; }
        public BankAccountTypeEnum BankAccountType { get; set; }

        public Guid BankId { get; set; }
        public virtual Bank Bank { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }

        public Guid AssociateAccount { get; set; }
        public string AssociateAccountName { get; set; }
        public string AssociateAccountType { get; set; }

        public Guid ReferenceId { get; set; }
        public string ReferenceName { get; set; }
        public string ReferenceType { get; set; }
    }
}
