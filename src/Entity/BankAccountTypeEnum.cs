﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public enum BankAccountTypeEnum
    {
        SavingsAccount,
        CheckingAccount
    }
}
