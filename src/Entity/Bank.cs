﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class Bank : BaseEntity
    {
        public Guid BankId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<BankAccount> BankAccounts { get; set; }
    }
}
