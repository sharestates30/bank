﻿using Core;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity.Configuration
{
    public static class DbInitializer
    {
        public static void Initialize(BankDbContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
